<?php

namespace App\Model\User;


use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{
    private $container;

    const SOC_NETWORK_VKONTAKTE = "vkontakte";
    const SOC_NETWORK_FACEBOOK = "facebook";
    const SOC_NETWORK_GOOGLE = "google";

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     */
    public function createNewUser(array $data, bool $encodePassword = true ) {
        $user = new User();
        $user->setEmail($data['email']);
        $user->setPassport($data['passport']);
        $user->setVkId($data['vkId'] ?? null);
        $user->setFaceBookId($data['faceBookId'] ?? null);
        $user->setGoogleId($data['googleId'] ?? null);
        if($encodePassword) {
            $password = $this->encodePassword($data['password']);
        } else {
            $password = $data['password'];
        }
        $user->setPassword($password);

        return $user;
    }


    /**
     * @param string $password
     * @return string
     */
    public function encodePassword(string $password) {
        return md5($password).md5($password.'3');
    }

    public function makeUserSession(User $user) {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }

}