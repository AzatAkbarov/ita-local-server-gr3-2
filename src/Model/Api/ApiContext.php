<?php

namespace App\Model\Api;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_CONCRETE_CLIENT_BY_EMAIL = '/client_by_email/email/{email}';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/check_client_credentials/{passwordHash}/{email}';
    const ENDPOINT_CHECK_CLIENT_BY_NETWORK_UID = '/check_client_by_network_uid/{network}/{uid}';
    const ENDPOINT_EDIT_CONCRETE_CLIENT_ADD_SOC_BINDING = '/client/edit/add_soc_binding';
    const ENDPOINT_CONCRETE_CLIENT_BY_NETWORK_UID = '/concrete_client_by_network_uid/{network}/{uid}';


    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
            'passport' => $passport,
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $password
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function checkClientCredentials(string $password, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_CREDENTIALS, [
            'passwordHash' => $this->userHandler->encodePassword($password),
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $network
     * @param string $uid
     * @return bool
     * @throws ApiException
     */
    public function checkClientByNetworkUId(string $network, string $uid)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_BY_NETWORK_UID, [
            'network' => $network,
            'uid' => $uid
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param $network
     * @param $uid
     * @return array
     * @throws ApiException
     */
    public function getClientByNetworkUId($network, $uid)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_NETWORK_UID, [
            'network' => $network,
            'uid' => $uid
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param $email
     * @return array
     * @throws ApiException
     */
    public function getClientByEmail($email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function addSocialBindingToTheClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_EDIT_CONCRETE_CLIENT_ADD_SOC_BINDING, self::METHOD_PUT, $data);
    }
}