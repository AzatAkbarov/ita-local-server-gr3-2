<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UloginRegisterType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class ULoginController extends Controller
{
    const U_LOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/profile/ulogin_data_back", name="link_social_network")
     * @param ObjectManager $manager
     * @param ApiContext $apiContext
     * @param Session $session
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws ApiException
     */
    public function linkingAnAccountAction(ObjectManager $manager, ApiContext $apiContext, Session $session)
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($s, true);
        $network = $user['network'];
        $uId = $user['uid'];

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $data = [
            'email' => $currentUser->getEmail(),
            'network' => $network,
            'uid' => $uId
        ];
        $error = null;
        try {
            if ($apiContext->addSocialBindingToTheClient($data)) {
                $currentUser = $this->getUser();
                $currentUser->setSocialId($network, $uId);
                $manager->persist($currentUser);
                $manager->flush();
                $session->getFlashBag()->set('message', "Вы успешно привязали к своему аккаунту аккунт из соц сети {$network}");
                return $this->redirectToRoute('profile');
            } else {
                $error = 'Вас нету в базе,произошла неизвестная ошибка!';
            }
        } catch (ApiException $e) {
            if(isset($e->getResponse()['status']) && $e->getResponse()['status'] === 'fail') {
                $userDataFromCentral = $apiContext->getClientByEmail($currentUser->getEmail());
                $currentUser->setFaceBookId($userDataFromCentral['faceBookId'])
                    ->setVkId($userDataFromCentral['vkId'])
                    ->setGoogleId($userDataFromCentral['googleId']);
                $manager->persist($currentUser);
                $manager->flush();
            }
            $error = $e->getResponse()['error'];
        }
        $session->getFlashBag()->set('error', $error);
        return $this->redirectToRoute('profile');
    }

    /**
     * @Route("/authentication_with_soc_network", name="authentication_with_soc_network")
     * @param ObjectManager $manager
     * @param ApiContext $apiContext
     * @param Session $session
     * @param UserRepository $userRepository
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function authenticationWithSocialAction(
        Session $session,
        ObjectManager $manager,
        ApiContext $apiContext,
        UserRepository $userRepository,
        UserHandler $userHandler)
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);
        $network = $userData['network'];
        $uid = $userData['uid'];

        $user = $userRepository->findOneByUid($network, $uid);
        if($user) {
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('homepage');
        } else {
            try {
                if ($apiContext->checkClientByNetworkUId($network, $uid)) {
                    $userDataFromCentral = $apiContext->getClientByNetworkUId($network, $uid);
                    $user = $userRepository->findOneByPassportAndEmail($userDataFromCentral['passport'], $userDataFromCentral['email']);
                    if($user) {
                        $user->setSocialId($network, $uid);
                    } else {
                        $user = $userHandler->createNewUser($userDataFromCentral, false);
                    }
                    $manager->persist($user);
                    $manager->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('homepage');
                } else {
                    $session->getFlashBag()->set('error', 'Вы еще не регистрировались у нас,либо не привязали данную соц сеть к своему аккаунту');
                }
            } catch (ApiException $e) {
            }
        }
        return $this->redirectToRoute('authentication');
    }


    /**
     * @Route("/ulogin/register_social_network", name="ulogin_register")
     */
    public function registerCaseAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = new User();
        if(isset($userData['email'])) {
            $user->setEmail($userData['email']);
        }
        $form = $this->createForm(
            UloginRegisterType::class, $user, [
                'action' => $this
                    ->get('router')
                    ->generate('ulogin_register-case2')
            ]
        );
        $this->get('session')->set(self::U_LOGIN_DATA, $s);

        return $this->render('ulogin_registration.html.twig', [
            'form' => $form->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/ulogin/social_register-case2", name="ulogin_register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::U_LOGIN_DATA),
            true
        );

        $user = new User();

        $form = $this->createForm(
            UloginRegisterType::class,
            $user
        );

        $form->handleRequest($request);
        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail()) || ($apiContext->checkClientByNetworkUId($userData['network'], $userData['uid']))) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);

                    $manager->persist($user);
                    $manager->flush();

                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('homepage');
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('ulogin_registration.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

}