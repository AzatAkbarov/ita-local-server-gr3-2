<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('index.html.twig');
    }

    /**
     * @Route("/authentication", name="authentication")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function userAuthenticationAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler,
        ApiContext $apiContext,
        ObjectManager $manager)
    {

        $form = $this->createForm("App\Form\LoginType");

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $email = $data['email'];
            $password = $data['password'];

            /** @var User $user || null */
            $user = $userRepository->findByPasswordAndEmail($email, $password);

            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('homepage');
            } else {
                try {
                    if ($apiContext->checkClientCredentials($password, $email)) {
                        $userDataFromCentral = $apiContext->getClientByEmail($email);

                        $user = $userHandler->createNewUser($userDataFromCentral, false);
                        $manager->persist($user);
                        $manager->flush();
                        $userHandler->makeUserSession($user);
                        return $this->redirectToRoute('homepage');
                    } else {
                        $error = 'Вас нету в базе,зарегистрируйтесь';
                    }
                } catch (ApiException $e) {
                }
            }
        }
        return $this->render('authentication.html.twig', array(
            "form" => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/registration", name="registration")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function userRegistrationAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword()
                    ];

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('homepage');
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('registration.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/profile", name="profile")
     * @return Response
     */
    public function myProfileAction()
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        return $this->render('profile.html.twig', [
            'vk' => $currentUser->getVkId() ? true : null,
            'facebook' => $currentUser->getFaceBookId() ? true : null,
            'google' => $currentUser->getGoogleId() ? true : null,
        ]);
    }
}