<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class UloginRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email',EmailType::class, [
            'label' => 'Ваш емайл:',
            'attr' => [
                'placeholder' => 'Введите адрес Вашей электронной почты'
            ]
        ])
            ->add('passport', TextType::class,[
                'label' => "Ваши пасспортные данные:",
                'attr' => [
                    'placeholder' => 'Введите Ваши пасспортные данные'
                ]
            ])
            ->add('password', PasswordType::class,[
                'label' => "Ваши пароль:",
                'attr' => [
                    'placeholder' => 'Введите пароль для обычной авторизации'
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Сохранить'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_u_login_register_type';
    }
}